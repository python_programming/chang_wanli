import math


def quadratic(a, b, c):
    for x in (a, b, c):
        if not isinstance(x, (int, float)):
            raise TypeError('Bad operated type')

    s = b * b - 4 * a * c
    if s > 0 and a != 0:
        X1 = (-b + math.sqrt(s)) / (2 * a)
        X2 = (b + math.sqrt(s)) / (2 * a)
        return (X1, X2)
    elif s == 0 and a != 0:
        X = -b / (2 * a)
        return X
    elif a == 0 and b != 0:
        X = -b / c
        return X
    elif a == 0 and b == 0:
        print('方程式无解')
    else:
        return ('方程式无解')


num = quadratic(1, -6, 5)
print(num)
