# -*- coding: utf-8 -*-
"""
文件名：减 乘 除
描  述：调用运算函数文件
作  者：20192307
日  期：2020/04/16
"""


def sum(a, b):
    x = a + b
    return x


def sub(a, b):
    x = a - b
    return x


def multiply(a, b):
    x = a * b
    return x


def divide(a, b):
    x = a / b
    return x


def remainder(a, b):
    x = a % b
    return x
