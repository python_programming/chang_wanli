# -*- coding: utf-8 -*-
"""
文件名：Python语言基础实践2
描  述：云班课第二次作业
作  者：20192307
日  期：2020/03/11
"""
'''
决策树（Decision tree）是一种特殊的树结构，由一个决策图和可能的结果（例如成本和风险）组成，
用来辅助决策。机器学习中，决策树是一个预测模型，决策树是机器学习十大算法之一，树中每个节点表
示某个对象，而每个分叉路径则代表某个可能的属性值，而每个叶节点则对应从根节点到该叶节点所经历
的路径所表示的对象的值。决策树仅有单一输出，通常该算法用于解决分类问题。
'''
'''
自己设计一个决策树，可以参考附图，例如心理测试（输入几个问题答案来测试）、电话系统（按1普通
按2English之类），使用for或者while循环，使用if判定语句，编写程序并运行将结果截图加水印传到
这里，代码传码云并贴出码云链接。（本题5分，无码云链接扣1分）
'''
# 就业决策树
none = True
while none:
    salary = int(
        input(
            "Please input the salary that the company can offer(Unit:Dollar):\n"
        ))
    if salary >= 50000:
        print("The salary is " + str(salary) +
              ",this company can offer the expected salary")
        workhour = int(input("Please input the work hours everyday:\n"))
        if workhour <= 8:
            print("The work hours are " + str(workhour) +
                  ",this company meet your demands")
            freecoffee = input(
                "Whether the company offer free coffee?(Yes or No)\n")
            if freecoffee.lower() == "yes" or freecoffee.lower() == "No":
                print("This company offer free coffer.Accept the offer!")
                none = False
            else:
                print("This company don't offer free coffer.Reject the offer!")
        else:
            print("The work hours are" + str(workhour) + ",Reject the offer!")
    else:
        print("The salary is " + str(salary) + ",Reject the offer!")
