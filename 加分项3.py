# 3.使用xlwt模块，生成一些数据，保存到一个excel文件中。可参考课堂演示。
import xlwt

workbook = xlwt.Workbook(encoding='ascii')
worksheet = workbook.add_sheet('My Worksheet')
style = xlwt.XFStyle()  # 初始化样式
font = xlwt.Font()  # 为样式创建字体
font.name = 'Times New Roman'
font.bold = True  # 黑体
font.underline = True  # 下划线
font.italic = True  # 斜体字
style.font = font  # 设定样式
worksheet.write(0, 0, 'Unformatted value')  # 不带样式的写入

worksheet.write(1, 0, 'Formatted value', style)  # 带样式的写入

workbook.save('加分项3.xls')  # 保存文件
