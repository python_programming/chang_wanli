# -*- coding: utf-8 -*-
"""
文件名：Python实验2
描  述：计算器
作  者：20192307
日  期：2020/04/16
"""
import math
from function_modules import sum, sub, divide, multiply, remainder

operator = input("您想要进行什么运算：加 减 乘 除 求余 开方(对a) 乘方\n")
a = int(input("请输入数字a:"))
b = int(input("请输入数字b:"))
if operator == "加":
    print("a+b =", sum(a, b))
if operator == "减":
    print("a-b =", sub(a, b))
if operator == "乘":
    print("a*b =", multiply(a, b))
if operator == "除":
    print("a/b =", divide(a, b))
if operator == "求余":
    print("a除以b的余数 =", remainder(a, b))
if operator == "开方":
    print("a的平方根 =", math.sqrt(a))
if operator == "乘方":
    print("a的b次方 =", math.pow(a, b))
