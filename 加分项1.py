# 1.使用Python-docx做一个word文档，可参考。送分题（加1分）
# https://blog.csdn.net/weixin_45523154/article/details/101715076
import os
from docx import Document
os.system("cd E:\\我的大学")
Docx = Document()
Docx.add_heading("一级标题", level=1)
Docx.add_paragraph("副级标题", "Title")
A = Docx.add_paragraph("My name is cwl")
A.add_run("学习python!!!")
Docx.add_heading("二级标题", level=2)
A = Docx.add_paragraph("二级标题内容")
B = A.add_run("二级标题正文 ")
B.font.bold = True
B.font.size = (20)
Docx.add_heading("我爱学习Python", level=3)
Docx.add_table(rows=5, cols=5)
Docx.save("Python.docx")
