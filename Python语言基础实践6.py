# -*- coding: utf-8 -*-
"""
文件名：Python语言基础实践6
描  述：云班课第六次作业
作  者：20192307
日  期：2020/04/07
"""
"""
定义一个类，课程、学生等，要求输出相关对象信息。
（1）要求使用封装属性
（2）要求使用继承属性
（3）要求定义类的方法（不仅仅使用__init__()）
本题5分。
"""


class BestiStudent:
    '''电科院人'''
    department = ' '
    grade = ' '

    def __init__(self, department, grade):
        print("此学生是" + department + grade)

    def setName(self, name, age):
        BestiStudent.name = name
        BestiStudent.age = age
        print("姓名：" + BestiStudent.name + " 年龄：" + BestiStudent.age)


student1 = BestiStudent("网络空间安全系", "18级")
student1.setName("TZY", "18岁")


class Dormitory(BestiStudent):
    dorm = " "

    def __init__(self, dorm):
        super(BestiStudent, self).__init__()
        print("此学生寝室为" + dorm)


student2 = Dormitory("育才212")
student2.setName("CWL", "19岁")
