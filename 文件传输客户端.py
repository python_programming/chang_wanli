# -*- coding: utf-8 -*-
"""
文件名：Python语言基础实践9
描  述：云班课第九次作业
作  者：20192307
日  期：2020/05/07
"""
import socket
import os
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8001))
str1 = input("请输入要传输的文件名：")
s.sendall(str1.encode())
os.chdir(r"E:\\学习\\python程序设计\\PYTHON_TEST")
file = open(str1, 'r')
text = file.read()
s.sendall(text.encode())
file.close()
data = s.recv(1024)
print("来自 ('127.0.0.1') 的信息：", data.decode())
s.sendall("收到".encode())
name = s.recv(1024)
print("来自 ('127.0.0.1') 的文件：", name.decode())
data = s.recv(1024)
f = open("reply.txt", "w")
f.write(data.decode())
f.close()
print("文件内容为：", data.decode())
s.sendall("已成功接收，中断连接！".encode())
s.close()
