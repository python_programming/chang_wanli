# -*- coding: utf-8 -*-
"""
文件名：Python语言基础实践8.2
描  述：云班课第八次作业
作  者：20192307
日  期：2020/04/29
"""
# import os.path
import pymysql
conn = pymysql.connect("besti1923")
# 创建一个cursor：游标对象
cursor = conn.cursor()
# 1、创建一个表
cursor.execute(
    'create table if not exists student(number int(10)primary key,name varchar(20),score float)'
)
# 2、插入三条记录
cursor.execute(
    'insert into student (number, name, score) values(20192307,"CWL",95.00)')
cursor.execute(
    'insert into student (number, name, score) values(20192309,"JYF",90.00)')
cursor.execute(
    'insert into student (number, name, score) values(20192312,"TZY",99.00)')
# 3、查询select语句
cursor.execute('select * from student')
print(cursor.fetchall())
# 4、修改一条记录
cursor.execute('update student set score=92 where number = 20192309')
cursor.execute('select * from student')
print(cursor.fetchall())
# 5、删除一条记录
cursor.execute('delete from student where number = 20192309')
cursor.execute('select * from student')
print(cursor.fetchall())
# 6、关闭数据库
conn.close()
