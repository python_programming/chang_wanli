# -*- coding: utf-8 -*-
"""
文件名：Python语言基础实践9
描  述：云班课第九次作业
作  者：20192307
日  期：2020/05/06
"""
import socket
# 服务器端socket初始化
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8001))
# 绑定，localhost本机地址，端口
s.listen()
conn, address = s.accept()
data = conn.recv(1024)
print(data.decode())
conn.sendall(("服务器已经接受到了数据内容：" + str(data.decode())).encode())
s.close()
