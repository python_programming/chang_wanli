# -*- coding: utf-8 -*-
"""
文件名：Python语言基础实践9
描  述：云班课第九次作业
作  者：20192307
日  期：2020/05/06
"""
import socket
# 客户端socket初始化
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 参数1：服务器之间的网络通信；参数2：流式socket，for TCP
s.connect(('127.0.0.1', 8001))
# 连接，元组的形式，(IP地址，端口)
str = input("请输入传输内容：")
s.sendall(str.encode())
date = s.recv(1024)
print(date)
s.close()
