# 2.建立一个word文件，可以是上面的文件（命名“明文.doc”，建议纯文字，不要加图片），
# 使用DES加密算法加密并保存到“密文.doc”文件中。然后再解密，解密后文件“解密文件.doc”。
# 加密算法的使用可以参考下面的链接：
# https://blog.csdn.net/yangxiaodong88/article/details/80801278
import os
from docx import Document
from Cryptodome.Cipher import DES
os.system("cd E:\\我的大学")
Docx = Document()
Docx.add_heading("first", level=1)
Docx.add_paragraph("next", "Title")
A = Docx.add_paragraph("My name is cwl\n")
A.add_run("I learn python!!!")
Docx.add_heading("second", level=2)
A = Docx.add_paragraph("story")
B = A.add_run("hahahahaha")
B.font.bold = True
B.font.size = (20)
Docx.add_heading("I love Python", level=3)
Docx.save("明文.doc")

os.chdir("E:\\我的大学\\学习\\Python程序设计\\Python_test")
file = open("明文.doc", "rb")
print("文件名为: ", file.name)
text = file.readlines()
print(text)
key = b'abcdefgh'


def pad(text):
    while len(text) % 8 != 0:
        text += ' '
    return text


des = DES.new(key, DES.MODE_ECB)  # 创建一个DES实例
padded_text = pad(text)
encrypted_text = des.encrypt(padded_text.encode('utf-8'))  # 加密
print(encrypted_text)
plain_text = des.decrypt(encrypted_text).decode().rstrip(' ')  # 解密
print(plain_text)
