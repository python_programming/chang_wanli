# -*- coding: utf-8 -*-
"""
文件名：Python语言基础实践9
描  述：云班课第九次作业
作  者：20192307
日  期：2020/05/07
"""
import socket
import os
os.chdir(r"E:\\学习\\python程序设计\\PYTHON_TEST")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8001))
s.listen()
conn, address = s.accept()
name = conn.recv(1024)
print("来自", address, "的文件：", name.decode())
data = conn.recv(1024)
f = open("receive.txt", "w")
f.write(data.decode())
f.close()
print("来自", address, "的信息：", data.decode(), "已保存为receive.txt")
conn.sendall("服务器已经收到了数据内容,准备传输文件，注意接收！".encode())
data = conn.recv(1024)
conn.sendall("reply.txt".encode())
f = open("receive.txt", "r")
data = f.read()
conn.sendall(data.encode())
f.close()
data = conn.recv(1024)
print("来自", address, "的信息：", data.decode())
s.close()
